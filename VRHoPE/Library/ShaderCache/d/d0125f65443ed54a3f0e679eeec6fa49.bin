2O                         BEAUTIFY_DALTONIZE     BEAUTIFY_BLOOM     BEAUTIFY_TONEMAP_ACES      BEAUTIFY_VIGNETTING    BEAUTIFY_FRAME  >!     xlatMtlMain #include <metal_stdlib>
#include <metal_texture>
using namespace metal;
struct Globals_Type
{
    float4 _ScreenParams;
    float4 _ZBufferParams;
    float4 _MainTex_TexelSize;
    float4 _ColorBoost;
    float4 _Sharpen;
    float4 _Dither;
    float4 _TintColor;
    float4 _Bloom;
    float4 _Vignetting;
    float _VignettingAspectRatio;
    float4 _Frame;
};

struct Mtl_FragmentIn
{
    float2 TEXCOORD0 [[ user(TEXCOORD0) ]] ;
    float4 TEXCOORD1 [[ user(TEXCOORD1) ]] ;
};

struct Mtl_FragmentOut
{
    float4 SV_Target0 [[ color(0) ]];
};

fragment Mtl_FragmentOut xlatMtlMain(
    constant Globals_Type& Globals [[ buffer(0) ]],
    texture2d<half, access::sample > _MainTex [[ texture (0) ]] ,
    sampler sampler_MainTex [[ sampler (0) ]] ,
    texture2d<float, access::sample > _CameraDepthTexture [[ texture (1) ]] ,
    sampler sampler_CameraDepthTexture [[ sampler (1) ]] ,
    texture2d<half, access::sample > _BloomTex [[ texture (2) ]] ,
    sampler sampler_BloomTex [[ sampler (2) ]] ,
    Mtl_FragmentIn input [[ stage_in ]])
{
    Mtl_FragmentOut output;
    float4 u_xlat0;
    half3 u_xlat16_0;
    float3 u_xlat1;
    half3 u_xlat16_1;
    float4 u_xlat2;
    half4 u_xlat16_2;
    float3 u_xlat3;
    half3 u_xlat16_3;
    float2 u_xlat4;
    half u_xlat16_4;
    bool2 u_xlatb4;
    float3 u_xlat5;
    float u_xlat8;
    half u_xlat16_8;
    float u_xlat9;
    half u_xlat16_9;
    float u_xlat12;
    half u_xlat16_12;
    float u_xlat13;
    half u_xlat16_13;
    u_xlat0.x = float(0.0);
    u_xlat0.w = float(0.0);
    u_xlat0.yz = Globals._MainTex_TexelSize.yx;
    u_xlat1.xyz = (-u_xlat0.xyx) + input.TEXCOORD1.xyw;
    u_xlat1.x = _CameraDepthTexture.sample(sampler_CameraDepthTexture, u_xlat1.xy, level(u_xlat1.z)).x;
    u_xlat1.x = Globals._ZBufferParams.x * u_xlat1.x + Globals._ZBufferParams.y;
    u_xlat1.x = float(1.0) / u_xlat1.x;
    u_xlat5.xyz = u_xlat0.xyx + input.TEXCOORD1.xyw;
    u_xlat5.x = _CameraDepthTexture.sample(sampler_CameraDepthTexture, u_xlat5.xy, level(u_xlat5.z)).x;
    u_xlat5.x = Globals._ZBufferParams.x * u_xlat5.x + Globals._ZBufferParams.y;
    u_xlat5.x = float(1.0) / u_xlat5.x;
    u_xlat9 = max(u_xlat1.x, u_xlat5.x);
    u_xlat1.x = min(u_xlat1.x, u_xlat5.x);
    u_xlat2.xyz = (-u_xlat0.zww) + input.TEXCOORD1.xyw;
    u_xlat5.x = _CameraDepthTexture.sample(sampler_CameraDepthTexture, u_xlat2.xy, level(u_xlat2.z)).x;
    u_xlat5.x = Globals._ZBufferParams.x * u_xlat5.x + Globals._ZBufferParams.y;
    u_xlat5.x = float(1.0) / u_xlat5.x;
    u_xlat9 = max(u_xlat5.x, u_xlat9);
    u_xlat2.xyz = u_xlat0.zww + input.TEXCOORD1.xyw;
    u_xlat13 = _CameraDepthTexture.sample(sampler_CameraDepthTexture, u_xlat2.xy, level(u_xlat2.z)).x;
    u_xlat13 = Globals._ZBufferParams.x * u_xlat13 + Globals._ZBufferParams.y;
    u_xlat13 = float(1.0) / u_xlat13;
    u_xlat9 = max(u_xlat13, u_xlat9);
    u_xlat1.x = min(u_xlat5.x, u_xlat1.x);
    u_xlat1.x = min(u_xlat13, u_xlat1.x);
    u_xlat1.x = (-u_xlat1.x) + u_xlat9;
    u_xlat1.x = u_xlat1.x + 9.99999975e-06;
    u_xlat1.x = Globals._Sharpen.y / u_xlat1.x;
    u_xlat1.x = clamp(u_xlat1.x, 0.0f, 1.0f);
    u_xlat2 = u_xlat0.zwxy + input.TEXCOORD0.xyxy;
    u_xlat0 = (-u_xlat0) + input.TEXCOORD0.xyxy;
    u_xlat16_3.xyz = _MainTex.sample(sampler_MainTex, u_xlat2.zw).xyz;
    u_xlat16_2.xyz = _MainTex.sample(sampler_MainTex, u_xlat2.xy).xyz;
    u_xlat16_9 = dot(u_xlat16_2.xyz, half3(0.298999995, 0.587000012, 0.114));
    u_xlat16_13 = dot(u_xlat16_3.xyz, half3(0.298999995, 0.587000012, 0.114));
    u_xlat16_2.xyz = _MainTex.sample(sampler_MainTex, u_xlat0.xy).xyz;
    u_xlat16_0.xyz = _MainTex.sample(sampler_MainTex, u_xlat0.zw).xyz;
    u_xlat16_0.x = dot(u_xlat16_0.xyz, half3(0.298999995, 0.587000012, 0.114));
    u_xlat16_4 = dot(u_xlat16_2.xyz, half3(0.298999995, 0.587000012, 0.114));
    u_xlat16_8 = min(u_xlat16_4, u_xlat16_13);
    u_xlat16_4 = max(u_xlat16_4, u_xlat16_13);
    u_xlat16_4 = max(u_xlat16_0.x, u_xlat16_4);
    u_xlat16_0.x = min(u_xlat16_0.x, u_xlat16_8);
    u_xlat16_0.x = min(u_xlat16_9, u_xlat16_0.x);
    u_xlat16_4 = max(u_xlat16_9, u_xlat16_4);
    u_xlat16_0.x = half(float(u_xlat16_0.x) + -9.99999997e-07);
    u_xlat16_8 = (-u_xlat16_0.x) + u_xlat16_4;
    u_xlat8 = Globals._Sharpen.w / float(u_xlat16_8);
    u_xlat8 = clamp(u_xlat8, 0.0f, 1.0f);
    u_xlat16_2 = _MainTex.sample(sampler_MainTex, input.TEXCOORD0.xy);
    u_xlat16_12 = dot(u_xlat16_2.xyz, half3(0.298999995, 0.587000012, 0.114));
    u_xlat16_0.x = u_xlat16_12 * half(2.0) + (-u_xlat16_0.x);
    u_xlat16_0.x = (-u_xlat16_4) + u_xlat16_0.x;
    u_xlat0.x = u_xlat8 * float(u_xlat16_0.x);
    u_xlat0.x = u_xlat1.x * u_xlat0.x;
    u_xlat0.x = u_xlat0.x * Globals._Sharpen.x;
    u_xlat0.x = max(u_xlat0.x, (-Globals._Sharpen.z));
    u_xlat0.x = min(u_xlat0.x, Globals._Sharpen.z);
    u_xlat4.x = u_xlat5.x + (-Globals._Dither.z);
    u_xlatb4.y = u_xlat5.x>=Globals._Dither.y;
    u_xlatb4.x = abs(u_xlat4.x)<Globals._Dither.w;
    u_xlat4.xy = select(float2(0.0, 0.0), float2(1.0, 1.0), bool2(u_xlatb4.xy));
    u_xlat0.x = u_xlat0.x * u_xlat4.x + 1.0;
    u_xlat1.xy = input.TEXCOORD0.xy * Globals._ScreenParams.xy;
    u_xlat4.x = dot(float2(171.0, 231.0), u_xlat1.xy);
    u_xlat1.xyz = u_xlat4.xxx * float3(0.00970873795, 0.0140845068, 0.010309278);
    u_xlat1.xyz = fract(u_xlat1.xyz);
    u_xlat1.xyz = u_xlat1.xyz + float3(-0.5, -0.5, -0.5);
    u_xlat1.xyz = u_xlat4.yyy * u_xlat1.xyz;
    u_xlat1.xyz = u_xlat1.xyz * Globals._Dither.xxx + float3(1.0, 1.0, 1.0);
    u_xlat1.xyz = u_xlat1.xyz * float3(u_xlat16_2.xyz);
    output.SV_Target0.w = float(u_xlat16_2.w);
    u_xlat2.xyz = u_xlat1.xyz;
    u_xlat2.xyz = clamp(u_xlat2.xyz, 0.0f, 1.0f);
    u_xlat2.xyz = (-u_xlat2.xyz) + float3(1.0, 1.0, 1.0);
    u_xlat3.xyz = u_xlat1.xyz * u_xlat2.yxx;
    u_xlat2.xyz = u_xlat2.zzy * u_xlat3.xyz;
    u_xlat2.xyz = u_xlat2.xyz * Globals._ColorBoost.www + float3(1.0, 1.0, 1.0);
    u_xlat1.xyz = u_xlat1.xyz * u_xlat2.xyz;
    u_xlat4.x = dot(u_xlat1.xyz, float3(0.298999995, 0.587000012, 0.114));
    u_xlat4.x = u_xlat4.x + 9.99999975e-05;
    u_xlat4.x = float(u_xlat16_12) / u_xlat4.x;
    u_xlat1.xyz = u_xlat4.xxx * u_xlat1.xyz;
    u_xlat2.xyz = u_xlat0.xxx * u_xlat1.xyz;
    u_xlat4.x = max(u_xlat2.z, u_xlat2.y);
    u_xlat4.x = max(u_xlat4.x, u_xlat2.x);
    u_xlat8 = min(u_xlat2.z, u_xlat2.y);
    u_xlat8 = min(u_xlat8, u_xlat2.x);
    u_xlat4.x = (-u_xlat8) + u_xlat4.x;
    u_xlat4.x = clamp(u_xlat4.x, 0.0f, 1.0f);
    u_xlat4.x = (-u_xlat4.x) + 1.0;
    u_xlat4.x = u_xlat4.x * Globals._ColorBoost.z;
    u_xlat8 = dot(u_xlat2.xyz, float3(0.298999995, 0.587000012, 0.114));
    u_xlat1.xyz = u_xlat1.xyz * u_xlat0.xxx + (-float3(u_xlat8));
    u_xlat0.xyz = u_xlat4.xxx * u_xlat1.xyz + float3(1.0, 1.0, 1.0);
    u_xlat16_1.xyz = _BloomTex.sample(sampler_BloomTex, input.TEXCOORD0.xy).xyz;
    u_xlat1.xyz = float3(u_xlat16_1.xyz) * Globals._Bloom.xxx;
    u_xlat0.xyz = u_xlat2.xyz * u_xlat0.xyz + u_xlat1.xyz;
    u_xlat1.xyz = float3(u_xlat16_12) * Globals._Vignetting.xyz + (-u_xlat0.xyz);
    u_xlat2.xyz = input.TEXCOORD0.xyx + float3(-0.5, -0.5, -0.5);
    u_xlat2.w = u_xlat2.y * Globals._VignettingAspectRatio;
    u_xlat12 = dot(u_xlat2.zw, u_xlat2.zw);
    u_xlat13 = max(abs(u_xlat2.y), abs(u_xlat2.x));
    u_xlat13 = u_xlat13 + (-Globals._Frame.w);
    u_xlat13 = u_xlat13 * 50.0;
    u_xlat13 = clamp(u_xlat13, 0.0f, 1.0f);
    u_xlat12 = u_xlat12 * Globals._Vignetting.w;
    u_xlat12 = clamp(u_xlat12, 0.0f, 1.0f);
    u_xlat0.xyz = float3(u_xlat12) * u_xlat1.xyz + u_xlat0.xyz;
    u_xlat0.xyz = u_xlat0.xyz * Globals._ColorBoost.xxx;
    u_xlat1.xyz = u_xlat0.xyz * float3(2.50999999, 2.50999999, 2.50999999) + float3(0.0299999993, 0.0299999993, 0.0299999993);
    u_xlat1.xyz = u_xlat0.xyz * u_xlat1.xyz;
    u_xlat2.xyz = u_xlat0.xyz * float3(2.43000007, 2.43000007, 2.43000007) + float3(0.589999974, 0.589999974, 0.589999974);
    u_xlat0.xyz = u_xlat0.xyz * u_xlat2.xyz + float3(0.140000001, 0.140000001, 0.140000001);
    u_xlat0.xyz = u_xlat1.xyz / u_xlat0.xyz;
    u_xlat1.xyz = u_xlat0.xyz * Globals._TintColor.xyz + (-u_xlat0.xyz);
    u_xlat0.xyz = Globals._TintColor.www * u_xlat1.xyz + u_xlat0.xyz;
    u_xlat0.xyz = u_xlat0.xyz + float3(-0.5, -0.5, -0.5);
    u_xlat0.xyz = u_xlat0.xyz * Globals._ColorBoost.yyy + float3(0.5, 0.5, 0.5);
    u_xlat1.xyz = (-u_xlat0.xyz) + Globals._Frame.xyz;
    output.SV_Target0.xyz = float3(u_xlat13) * u_xlat1.xyz + u_xlat0.xyz;
    return output;
}
                            Globals �         _ScreenParams                            _ZBufferParams                          _MainTex_TexelSize                           _ColorBoost                   0      _Sharpen                  @      _Dither                   P   
   _TintColor                    `      _Bloom                    p      _Vignetting                   �      _VignettingAspectRatio                    �      _Frame                    �         _MainTex              _CameraDepthTexture          	   _BloomTex               Globals            