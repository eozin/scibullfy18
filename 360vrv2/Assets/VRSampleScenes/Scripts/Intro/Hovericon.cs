﻿using UnityEngine;
using System.Collections;
using VRStandardAssets.Utils;

namespace VRStandardAssets.Intro
{
    // The intro scene takes users through the basics
    // of interacting through VR in the other scenes.
    // This manager controls the steps of the intro
    // scene.
    public class Hovericon : MonoBehaviour
    {
        [SerializeField] private Reticle m_Reticle;                         // The scene only uses SelectionSliders so the reticle should be shown.
        [SerializeField] private SelectionRadial m_Radial;                  // Likewise, since only SelectionSliders are used, the radial should be hidden.
        [SerializeField] private UIFader m_HowToUseFader;                   // This fader controls the UI showing how to use SelectionSliders.
        [SerializeField] private SelectionSlider m_HowToUseSlider;          // This is the slider that is used to demonstrate how to use them.
		//public bool Overbone;
		public MediaPlayerCtrl scrMedia;

		public bool m_bFinish = false;
		//public bool Overbone;


        private IEnumerator Start ()
        {
			//Overbone = ExampleInteractiveItem.overbone;
            m_Reticle.Show ();
            
            m_Radial.Show ();

            // In order, fade in the UI on how to use sliders, wait for the slider to be filled then fade out the UI.
            yield return StartCoroutine (m_HowToUseFader.InteruptAndFadeIn ());
            yield return StartCoroutine (m_HowToUseSlider.WaitForBarToFill ());
            yield return StartCoroutine (m_HowToUseFader.InteruptAndFadeOut ());
			//scrMedia.OnEnd += OnEnd;
			scrMedia.Pause();

        }

    }
}