﻿using UnityEngine;
using System.Collections;
using VRStandardAssets.Utils;


    // The intro scene takes users through the basics
    // of interacting through VR in the other scenes.
    // This manager controls the steps of the intro
    // scene.
    public class rightbone : MonoBehaviour
    {
        [SerializeField] private Reticle m_Reticle;                         // The scene only uses SelectionSliders so the reticle should be shown.
        [SerializeField] private SelectionRadial m_Radial;                  // Likewise, since only SelectionSliders are used, the radial should be hidden.
		[SerializeField] private UIFader m_HowToUseFader;                   // This fader controls the UI showing how to use SelectionSliders.
        [SerializeField] private SelectionSlider m_HowToUseSlider;          // This is the slider that is used to demonstrate how to use them.
        [SerializeField] private UIFader m_Boneright;            // Afterwards users are asked to confirm how to use sliders in this UI.
        [SerializeField] private SelectionSlider m_BonerightSlider;   // They demonstrate this using this slider.
		[SerializeField] private UIFader m_loop;   
		[SerializeField] private SelectionSlider m_loopSlider;   // They demonstrate this using this slider.
		[SerializeField] private UIFader m_exit;            // Afterwards users are asked to confirm how to use sliders in this UI.
		[SerializeField] private SelectionSlider m_exitSlider;   // They demonstrate this using this slider.

		//[SerializeField] private UIFader m_ReturnFader;                     // The final instructions are controlled using this fader.

		public MediaPlayerCtrl scrMedia;
		public static bool start=false;
	    public int myturn=0;
		public bool restart=true;
		public void Update(){

			if (restart) {
				restart = false;
				StartCoroutine (Myrestart ());  
			}


			if((scrMedia.GetCurrentState() == MediaPlayerCtrl.MEDIAPLAYER_STATE.END)){
				
				myturn++;	   
				if(myturn==1){
					StartCoroutine (myloopicon ()); 
					StartCoroutine (myexiticon ()); 
				}

			}


		}
        private IEnumerator Myrestart ()
        {
            m_Reticle.Show ();
            m_Radial.Show ();
			start = true;
			
            // In order, fade in the UI on how to use sliders, wait for the slider to be filled then fade out the UI.
            yield return StartCoroutine (m_HowToUseFader.InteruptAndFadeIn ());
            yield return StartCoroutine (m_HowToUseSlider.WaitForBarToFill ());
            yield return StartCoroutine (m_HowToUseFader.InteruptAndFadeOut ());

			start = false;
			scrMedia.Play();
			//scrMedia.SeekTo (90000);
			yield return new WaitForSeconds(24);
		    myturn = 0;
			ExampleInteractiveItem.overbone = true;

			//yield return StartCoroutine (m_ReturnFader.InteruptAndFadeIn ());


      }


		private IEnumerator myloopicon ()
		{   
			m_Reticle.Show ();
			m_Radial.Show ();

			yield return StartCoroutine (m_loop.InteruptAndFadeIn ());
			yield return StartCoroutine (m_loopSlider.WaitForBarToFill ());
			yield return StartCoroutine (m_loop.InteruptAndFadeOut ());
			yield return StartCoroutine (m_exit.InteruptAndFadeOut ());
			scrMedia.Play();
			//scrMedia.SeekTo (90000);
			yield return new WaitForSeconds(24);
		    myturn = 0;
			ExampleInteractiveItem.overbone = true;

		}

		private IEnumerator myexiticon ()
		{   

			m_Reticle.Show ();
			m_Radial.Show ();

			yield return StartCoroutine (m_exit.InteruptAndFadeIn ());
			yield return StartCoroutine (m_exitSlider.WaitForBarToFill ());
			yield return StartCoroutine (m_exit.InteruptAndFadeOut ());
			yield return StartCoroutine (m_loop.InteruptAndFadeOut ());

			restart = true;

		}
    }
