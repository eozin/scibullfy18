﻿using UnityEngine;
using System.Collections;
using VRStandardAssets.Utils;


[RequireComponent(typeof(MeshFilter))]
[RequireComponent(typeof(MeshRenderer))]

    public class middlebone : MonoBehaviour
    {
        [SerializeField] private Reticle m_Reticle;                         // The scene only uses SelectionSliders so the reticle should be shown.
        [SerializeField] private SelectionRadial m_Radial;                  // Likewise, since only SelectionSliders are used, the radial should be hidden.
        [SerializeField] private UIFader m_HowToUseFader;                   // This fader controls the UI showing how to use SelectionSliders.
        [SerializeField] private SelectionSlider m_HowToUseSlider;          // This is the slider that is used to demonstrate how to use them.
        [SerializeField] private UIFader1 m_Boneright;            // Afterwards users are asked to confirm how to use sliders in this UI.
        [SerializeField] private SelectionSlider1 m_BonerightSlider;   // They demonstrate this using this slider.
		[SerializeField] private UIFader m_loop;            // Afterwards users are asked to confirm how to use sliders in this UI.
		[SerializeField] private SelectionSlider m_loopSlider;   // They demonstrate this using this slider.
		[SerializeField] private UIFader m_exit;            // Afterwards users are asked to confirm how to use sliders in this UI.
		[SerializeField] private SelectionSlider m_exitSlider;   // They demonstrate this using this slider.

		public MediaPlayerCtrl scrMedia;
		public static bool overbone=false;
		public static bool starthover = false;
	    public bool restart=true;
	    public int myturn=0;
	    public static bool STOP=true;
	    public static bool start=false;
	    int p;
	    public static bool once=false;
		public void Update(){

			if (restart) {
				restart = false;
				StartCoroutine (Myrestart ());  
			}

			if (scrMedia.GetSeekPosition() > 24000) {
				
		        	if (SelectionSlider1.show) {
						if (middlebone.starthover) {
					       if(!once)
							 StartCoroutine (myhover ()); 
					    }
					} else {
				
					    StartCoroutine (myhovergonemiddle ());
						m_Boneright.Setalpha ();
				        if(!middlebone.overbone)
				          starthover = true;
					}
						
		   }


		    if((scrMedia.GetCurrentState() == MediaPlayerCtrl.MEDIAPLAYER_STATE.END)){
			    StartCoroutine (myhovergone ()); 
				myturn++;	   
				if(myturn==1){
					StartCoroutine (myloopicon ()); 
					StartCoroutine (myexiticon ()); 
				}

			}
		
		}

			private IEnumerator myhover ()
			{   
          		m_Boneright.SetVisible ();
			    middlebone.starthover = false;
				yield return StartCoroutine (m_Boneright.InteruptAndFadeIn ());
				yield return StartCoroutine (m_BonerightSlider.WaitForBarToFill ());
				yield return StartCoroutine (m_Boneright.InteruptAndFadeOut ());
				m_Boneright.SetInvisible();
				middlebone.overbone = true;
		        //scrMedia.Pause ();
		        ///p=scrMedia.GetSeekPosition ();
				yield return new WaitForSeconds(10);
		       // scrMedia.Play ();
		       // scrMedia.SeekTo (p);
			    overbone = false;
			    starthover = false;
		        STOP = true;
		        once = true;
		        m_Boneright.SetInvisible();
			}




        public IEnumerator Myrestart ()
		{
		    m_Boneright.SetInvisible ();    
			m_Reticle.Show ();
			m_Radial.Show ();
			start = true;
			starthover = false;
			// In order, fade in the UI on how to use sliders, wait for the slider to be filled then fade out the UI.
			yield return StartCoroutine (m_HowToUseFader.InteruptAndFadeIn ());
			yield return StartCoroutine (m_HowToUseSlider.WaitForBarToFill ());
			yield return StartCoroutine (m_HowToUseFader.InteruptAndFadeOut ());
			start = false;
			scrMedia.Play ();
			yield return new WaitForSeconds (24);
			myturn = 0;
			STOP = false;
			starthover = true;
			m_Boneright.Setalpha ();
		}	


		private IEnumerator myhovergonemiddle ()
		{   
			yield return StartCoroutine (m_Boneright.InteruptAndFadeOut ());

		}
		private IEnumerator myhovergone ()
		{   
		    STOP = true;
		    overbone = false;
			starthover = false;
			yield return StartCoroutine (m_Boneright.InteruptAndFadeOut ());
		    m_Boneright.SetInvisible();

		}

		private IEnumerator myloopicon ()
		{   
			m_Reticle.Show ();
			m_Radial.Show ();

			yield return StartCoroutine (m_loop.InteruptAndFadeIn ());
		    yield return StartCoroutine (m_loopSlider.WaitForBarToFill ());
			yield return StartCoroutine (m_loop.InteruptAndFadeOut ());
		    yield return StartCoroutine (m_exit.InteruptAndFadeOut ());

		    starthover = false;
		    start = false;
		    
		    scrMedia.Play();
			yield return new WaitForSeconds(24); 
		    myturn = 0;
			STOP = false;
			yield return StartCoroutine (m_Boneright.InteruptAndFadeIn ());
			yield return StartCoroutine (m_BonerightSlider.WaitForBarToFill ());
			yield return StartCoroutine (m_Boneright.InteruptAndFadeOut ());
			m_Boneright.SetInvisible();
			overbone = true;
			yield return new WaitForSeconds(10);
			//print (scrMedia.GetSeekPosition ());
			overbone = false;
			starthover = true;
		}

		private IEnumerator myexiticon ()
		{   
		    
			m_Reticle.Show ();
			m_Radial.Show ();

			yield return StartCoroutine (m_exit.InteruptAndFadeIn ());
			yield return StartCoroutine (m_exitSlider.WaitForBarToFill ());
			yield return StartCoroutine (m_exit.InteruptAndFadeOut ());
		    yield return StartCoroutine (m_loop.InteruptAndFadeOut ());

			restart = true;

		}
    }
