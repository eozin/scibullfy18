using UnityEngine;
using VRStandardAssets.Utils;
using System.Collections;

    // This script is a simple example of how an interactive item can
    // be used to change things on gameobjects by handling events.
    public class ExampleInteractiveItemicon : MonoBehaviour
    {
       // [SerializeField] private Material m_NormalMaterial;                
       //[SerializeField] private Material m_OverMaterial;                  
        //[SerializeField] private Material m_ClickedMaterial;               
        //[SerializeField] private Material m_DoubleClickedMaterial;         
        [SerializeField] private VRInteractiveItem m_InteractiveItem;
     //  [SerializeField] private Renderer m_Renderer;
	   // [SerializeField] private UIFader m_Boneright;            // Afterwards users are asked to confirm how to use sliders in this UI.
	  //  [SerializeField] private SelectionSlider m_BonerightSlider;   // They demonstrate this using this slider.
	    
	    // public MediaPlayerCtrl scrMedia;
			public void Awake ()
			{
				//m_Renderer.material = m_NormalMaterial;
				
			}


			public void OnEnable()
			{
				m_InteractiveItem.OnOver += HandleOver;
				m_InteractiveItem.OnOut += HandleOut;
				m_InteractiveItem.OnClick += HandleClick;
				m_InteractiveItem.OnDoubleClick += HandleDoubleClick;

			}


			public void OnDisable()
			{
				m_InteractiveItem.OnOver -= HandleOver;
				m_InteractiveItem.OnOut -= HandleOut;
				m_InteractiveItem.OnClick -= HandleClick;
				m_InteractiveItem.OnDoubleClick -= HandleDoubleClick;

			}




			public void Update(){

	
		}


//		private IEnumerator myhover ()
//		{   
//			m_Boneright.SetVisible ();
//		    middlebone.starthover = false;
//			yield return StartCoroutine (m_Boneright.InteruptAndFadeIn ());
//			yield return StartCoroutine (m_BonerightSlider.WaitForBarToFill ());
//			yield return StartCoroutine (m_Boneright.InteruptAndFadeOut ());
//			m_Boneright.SetInvisible();
//			middlebone.overbone = true;
//			yield return new WaitForSeconds(10);
//		    middlebone.overbone = false;
//		    middlebone.starthover = true;
//
//		}
//
			//Handle the Over event
			public void HandleOver()
			{
		      // m_Boneright.SetVisible ();
		      
		    }


			//Handle the Out event
			public void HandleOut()
			{
		        
			}


			//Handle the Click event
			public void HandleClick()
			{
				//			show = !(show);
				//            Debug.Log("Show click state");
				//            m_Renderer.material = m_ClickedMaterial;
				//			if(show)
				//			p_Renderer.material = p_ClickedMaterial;
				//		    else
				//			p_Renderer.material = p_NormalMaterial;
			}


			//Handle the DoubleClick event
			public void HandleDoubleClick()
			{
				//            Debug.Log("Show double click");
				//            m_Renderer.material = m_DoubleClickedMaterial;
				//			p_Renderer.material = p_DoubleClickedMaterial;
			}




    }

