using UnityEngine;
using VRStandardAssets.Utils;
[RequireComponent(typeof(MeshFilter))]
[RequireComponent(typeof(MeshRenderer))]
    // This script is a simple example of how an interactive item can
    // be used to change things on gameobjects by handling events.
    public class ExampleInteractiveItemglow : MonoBehaviour
    {
		[SerializeField] private Material m_NormalMaterial;                
		[SerializeField] private Material m_OverMaterial;                  
		[SerializeField] private Material m_ClickedMaterial;               
		[SerializeField] private Material m_DoubleClickedMaterial;         
		[SerializeField] private VRInteractiveItem m_InteractiveItem;
		[SerializeField] private Renderer m_Renderer;

	    public float speed = 30f;
		public MediaPlayerCtrl scrMedia;

		void Start() {

		   
		}

		public void Awake ()
		{
			m_Renderer.material = m_NormalMaterial;

			//p_Renderer.material = p_NormalMaterial;
		}


		public void OnEnable()
		{
			m_InteractiveItem.OnOver += HandleOver;
			m_InteractiveItem.OnOut += HandleOut;
			m_InteractiveItem.OnClick += HandleClick;
			m_InteractiveItem.OnDoubleClick += HandleDoubleClick;

		}


		public void OnDisable()
		{
			m_InteractiveItem.OnOver -= HandleOver;
			m_InteractiveItem.OnOut -= HandleOut;
			m_InteractiveItem.OnClick -= HandleClick;
			m_InteractiveItem.OnDoubleClick -= HandleDoubleClick;

		}

		public void Update(){

		   if (middlebone.overbone) {

			Renderer[] renderers = GetComponentsInChildren<Renderer>();
			foreach (var r in renderers)
			{
				m_Renderer.enabled = true;
				r.material = m_OverMaterial;
				//r.enabled = false; // like disable it for example. 
			}
			  //m.material=m_OverMaterial;
		
		      //m_Renderer.material = m_OverMaterial;
			  transform.Rotate (Vector3.up, speed * Time.deltaTime);

			} else {
			    
				Renderer[] renderers = GetComponentsInChildren<Renderer>();
				foreach (var r in renderers)
				{
				    m_Renderer.enabled = false;
					r.material = m_NormalMaterial;
					//r.enabled = false; // like disable it for example. 
				}
			    //m_Renderer.material = m_NormalMaterial;
			}
		}




        //Handle the Over event
        public void HandleOver()
        {

//		if (middlebone.overbone == true) {
//				m_Renderer.material = m_OverMaterial;
//			} else {
//				m_Renderer.material = m_NormalMaterial;
//			}
//

        }


        //Handle the Out event
        public void HandleOut()
        {
		  // middlebone.overbone = false;
           // Debug.Log("Show out state");

		   //m_Renderer.material = m_NormalMaterial;
		
        }


        //Handle the Click event
        public void HandleClick()
        {

            //Debug.Log("Show click state");

            //m_Renderer.material = m_ClickedMaterial;
		
        }


        //Handle the DoubleClick event
        public void HandleDoubleClick()
        {
            //Debug.Log("Show double click");
            //m_Renderer.material = m_DoubleClickedMaterial;

        }
    }

