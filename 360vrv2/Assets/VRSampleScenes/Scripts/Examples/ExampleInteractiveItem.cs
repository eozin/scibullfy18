using UnityEngine;
using VRStandardAssets.Utils;
using System.Collections;

    // This script is a simple example of how an interactive item can
    // be used to change things on gameobjects by handling events.
    public class ExampleInteractiveItem : MonoBehaviour
    {
        [SerializeField] private Material m_NormalMaterial;                
        [SerializeField] private Material m_OverMaterial;                  
        [SerializeField] private Material m_ClickedMaterial;               
        [SerializeField] private Material m_DoubleClickedMaterial;         
        [SerializeField] private VRInteractiveItem m_InteractiveItem;
        [SerializeField] private Renderer m_Renderer;


	    public bool trigger=false;
	    public bool fadeouttrigger=false;
	    public static bool overbone=false;
	    public static bool fadeoutglow=true;
	    public MediaPlayerCtrl scrMedia;
		public float speed = 60f;


	    int p;
		public void Update(){
				if (overbone == true) {
			        StartCoroutine(fadein()); 
<<<<<<< HEAD
			       
=======
	
>>>>>>> 05b8b9f325255180b6d00316a763fbb2ecdc1c58
			        overbone = false;

				}
				if (trigger == true) {

					Renderer[] renderers = GetComponentsInChildren<Renderer>();
					foreach (var r in renderers)
					{
			        	m_Renderer.enabled = true;
						r.material = m_OverMaterial;
						
					}

			        transform.Rotate (Vector3.up, speed * Time.deltaTime);
			 
				}else{
					Renderer[] renderers = GetComponentsInChildren<Renderer>();
					foreach (var r in renderers)
					{
				        m_Renderer.enabled = false;
						r.material = m_NormalMaterial;
					
					}
				
				}
		}

	private IEnumerator fadein(){
		fadeoutglow = false;
		trigger = false;
		yield return new WaitForSeconds (3);
		//scrMedia.Pause();
		//p = scrMedia.GetSeekPosition ();
		trigger = true;
		scrMedia.Pause();
		p = scrMedia.GetSeekPosition ();
		yield return new WaitForSeconds (10);
		trigger = false;
		fadeoutglow = true;
		//scrMedia.Play ();
		//scrMedia.SeekTo (p);
	}


    }

