﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;



public class blink : MonoBehaviour {
	public Renderer rend;
	[SerializeField] private Material m_NormalMaterial;  
	[SerializeField] private Material m_OverMaterial;      
	public float fYRot;
	public bool Overbone=false;
	public bool trigger=false;
	public MediaPlayerCtrl scrMedia;
	void Start() {
		rend = GetComponent<Renderer>();
		rend.material.shader = Shader.Find("MK/Glow/Selective/Mobile/Diffuse");
		rend.material = m_NormalMaterial;
	}
	void Update() {
		Overbone = ExampleInteractiveItem.overbone;
		//GameObject go = GameObject.Find ("bone/default/default_MeshPart0");
		//ExampleInteractiveItemglow speedController = go.GetComponent <ExampleInteractiveItem> ();

				

		if (ExampleInteractiveItem.fadeoutglow) {
			rend.material = m_NormalMaterial;
		} else {
			rend.material = m_OverMaterial;    
			float shininess = Mathf.PingPong (Time.time, 1.0F);
			rend.material.SetFloat ("_MKGlowPower", shininess);
		}

	}
}